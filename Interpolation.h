#pragma once
#ifndef _TESTTEMP_H_
#define _TESTTEMP_H_
/*
 * Interpolation methods
 *
 *	Linear
 *		std::lerp?
 *	Polynomial(Lagrange)
 *	Quadratic
 *	Cubic
 *		* Catmull-Rom
 *	Hermite
 *	Parabolic
 *
 *	TODO:
	 *	?Moving Average
	 *	?FIR (neon)
	 *	?Sinc (band limited)
 *
 * 	NOTES:
 *	http://paulbourke.net/miscellaneous/interpolation/
 *
 */

#include <type_traits>
#include <cstddef>
#include <vector>
#include <math.h>       /* fabs */

template <typename T>
class Interpolation
{
	static_assert(std::is_floating_point<T>::value, "This class is only meant to be used with floating point types.");

	public:
		Interpolation();
		Interpolation(bool wrapping);
		~Interpolation();
		int setup(bool wrapping);
		int cleanup();

		//auto lerp(T x, T y0,  T y1)
		T lerp(T x, T y0,  T y1)
		{
			return y0 + x * (y1 - y0);
		}

		/*
		 * Linear Interpolation
		 */
		template<typename A>
		T linearInterpolation(std::vector<T,A> const& vec, float index);

		template<size_t N>
		T linearInterpolation(T (&ptr)[N], float index);

		T linearInterpolation(float index);

		T linearInterpolation(T *ptr, unsigned int size,  float index);

		/*
		 * Quadratic Interpolation
		 */
		template<typename A>
		T quadraticInterpolation(std::vector<T,A> const& vec, float index);

		template<size_t N>
		T quadraticInterpolation(T (&ptr)[N], float index);

		T quadraticInterpolation(float index);

		T quadraticInterpolation(T *ptr, unsigned int size,  float index);

		/*
		 * Cubic Interpolation
		 */
		template<typename A>
		T cubicInterpolation(std::vector<T,A> const& vec, float index, bool catmullRom = true);

		template<size_t N>
		T cubicInterpolation(T (&ptr)[N], float index, bool catmullRom = true);

		T cubicInterpolation(float index, bool catmullRom = true);

		T cubicInterpolation(T *ptr, unsigned int size,  float index, bool catmullRom = true);

		/*
		 * Hermite Interpolation
		 */
		template<typename A>
		T hermiteInterpolation(std::vector<T,A> const& vec, float index, T tension, T bias);

		template<size_t N>
		T hermiteInterpolation(T (&ptr)[N], float index, T tension, T bias);

		T hermiteInterpolation(float index, T tension, T bias);

		T hermiteInterpolation(T *ptr, unsigned int size,  float index, T tension, T bias);

		/*
		 * Lagrange Interpolation
		 */
		template<typename A>
		T lagrangeInterpolation(std::vector<T,A> const& vec, float index, int order);

		template<size_t N>
		T lagrangeInterpolation(T (&ptr)[N], float index, int order);

		T lagrangeInterpolation(float index, int order);

		T lagrangeInterpolation(T *ptr, unsigned int size,  float index, int order);

		/*
		 * Parabolic Interpolation
		 */
		template<typename A>
		T parabolicInterpolation(std::vector<T,A> const& vec, float index);

		template<size_t N>
		T parabolicInterpolation(T (&ptr)[N], float index);

		T parabolicInterpolation(float index);

		T parabolicInterpolation(T *ptr, unsigned int size, float index);

		template<typename I>
		I boundIndex(I index, unsigned int size)
		{
			if(index < 0)
				index += size;
			while(index >= size)
				index -= size;

			return index;
		}

		template<typename I>
		I checkIndexWithinEdges(I index, unsigned int size, int interpolationOrder)
		{
			if(_containerPtr == nullptr || _edges[0] < 0 || _edges[1] < 0)
				calcEdges(size, interpolationOrder);

			if(index >= _edges[0] && index < _edges[1])
				return true;
			else
				return false;
		}

		void calcEdges(unsigned int size, int interpolationOrder)
		{
			//if(interpolationOrder > size)
				// Throw exception
			if(interpolationOrder == 1)
			{
				_edges[0] = 0;
				_edges[1] = size;
			}
			else
			{
				int edge = (int)(interpolationOrder/2.0);
				_edges[0] = edge + (int)(interpolationOrder - 2*edge);
				_edges[1] = size - edge;
			}
		}


		T * setContainer(T *ptr, unsigned int size)
		{
			_containerPtr = ptr;
			_containerSize = size;
			_edges[0] = -1;
			_edges[1] = -1;

			return _containerPtr;
		}

		template<typename A>
		T setContainer(std::vector<T,A> const& vec)
		{
			return setContainer((const T*) vec.data(), vec.size());
		}

		template<size_t N>
		T setContainer(T (&ptr)[N])
		{
			return setContainer((const T*) ptr, N*sizeof(T));
		}

	private:
		bool _wrap =  true;
		int _edges[2] = {-1, -1}; // container edges
		T *_containerPtr = nullptr;
		unsigned int _containerSize = 0;
};

template<typename T>
Interpolation<T>::Interpolation()
{
}

template<typename T>
Interpolation<T>::Interpolation(bool wrapping)
{
	setup(wrapping);
}

template<typename T>
int Interpolation<T>::setup(bool wrapping)
{
	_wrap = wrapping;
	return 0;
}

template<typename T>
int Interpolation<T>::cleanup()
{
	return 0;
}

template<typename T>
Interpolation<T>::~Interpolation()
{
}

/*
 * Linear Interpolation
 */
template<typename T>
T Interpolation<T>::linearInterpolation(T *ptr, unsigned int size,  float index)
{
	if(_wrap)
		index = boundIndex(index, size);
	else if( !checkIndexWithinEdges(index, size, 1) )
		return index; // or return -1; or else

	int pIdx = (int)index;

	int nIdx = pIdx + 1;
	nIdx = boundIndex(nIdx, size);

	float frac = index - pIdx;

	T pVal = ptr[pIdx];
	T nVal = ptr[nIdx];

	return lerp(frac, pVal, nVal);
}

template<typename T>
template<typename A>
T Interpolation<T>::linearInterpolation(std::vector<T,A> const& vec, float index)
{
	return linearInterpolation(vec.data(), vec.size(), index);
}

template<typename T>
template<size_t N>
T Interpolation<T>::linearInterpolation(T (&ptr)[N], float index)
{
	return linearInterpolation(&ptr, N*sizeof(T), index);
}

template<typename T>
T Interpolation<T>::linearInterpolation(float index)
{
	if(_containerPtr == nullptr || _containerSize == 0)
		return index; // or return -1; or else
	else
		return linearInterpolation( _containerPtr, _containerSize, index);
}

/*
 * Lagrange Interpolation
 */
template<typename T>
T Interpolation<T>::lagrangeInterpolation(T *ptr, unsigned int size,  float index, int order)
{
	//if(order >= size)
		// Throw exception
	if(_wrap)
		index = boundIndex(index, size);
	else if( !checkIndexWithinEdges(index, size, order) )
		return index; // or return -1; or else

	T val  = 0;

	index = boundIndex(index, size);

	for(unsigned int n=0; n<order; n++)
	{
		auto _n = boundIndex(n, size);
		T prod = 1;
		for(unsigned int m=0; m<order; m++)
		{
			if(n != m)
			{
				auto _m = boundIndex(m, size);
				prod *= (index - _m)/(_n - _m);

			}
		}
		val = val + prod * (ptr[_n]);
	}
	return val;
}

template<typename T>
template<typename  A>
T Interpolation<T>::lagrangeInterpolation(std::vector<T,A> const& vec, float index, int order)
{
	return lagrangeInterpolation(vec.data(), vec.size(), index, order);
}

template<typename T>
template<size_t N>
T Interpolation<T>::lagrangeInterpolation(T (&ptr)[N], float index, int order)
{
	return lagrangeInterpolation(&ptr, N*sizeof(T), index, order);
}

template<typename T>
T Interpolation<T>::lagrangeInterpolation(float index, int order)
{
	if(_containerPtr == nullptr || _containerSize == 0)
		return index; // or return -1; or else
	else
		return lagrangeInterpolation( _containerPtr, _containerSize, index, order);
}

/*
 * Quadratic Interpolation
 */
template<typename T>
T Interpolation<T>::quadraticInterpolation(T *ptr, unsigned int size,  float index)
{
	if(_wrap)
		index = boundIndex(index, size);
	else if( !checkIndexWithinEdges(index, size, 3) )
		return index; // or return -1; or else

	int pIdx = (int)index;

	int nIdx = pIdx + 1;
	nIdx = boundIndex(nIdx, size);

	int ppIdx = pIdx - 1;
	ppIdx = boundIndex(ppIdx, size);

	float frac = index - pIdx;

#if 0
	T ppVal = ptr[ppIdx];
	T pVal = ptr[pIdx];
	T nVal = ptr[nIdx];

	T _c1 = 0.5 * (nVal - ppVal);
	T _c2 = 0.5 * (nVal - 2 * pVal + ppVal);

	return _c2 * (frac * frac)  + _c1 * frac + pVal;
#endif

	T val[3] = { ptr[ppIdx], ptr[pIdx], ptr[nIdx] };

	T _c1 = 0.5 * (val[2] - val[0]);
	T _c2 = 0.5 * (val[2] - 2 * val[1] + val[0]);

	return _c2 * (frac * frac)  + _c1 * frac + val[1];
}

template<typename T>
template<typename A>
T Interpolation<T>::quadraticInterpolation(std::vector<T,A> const& vec, float index)
{
	return quadraticInterpolation(vec.data(), vec.size(), index);
}

template<typename T>
template<size_t N>
T Interpolation<T>::quadraticInterpolation(T (&ptr)[N], float index)
{
	return quadraticInterpolation(ptr, N*sizeof(T), index);
}

template<typename T>
T Interpolation<T>::quadraticInterpolation(float index)
{
	if(_containerPtr == nullptr || _containerSize == 0)
		return index; // or return -1; or else
	else
		return quadraticInterpolation(_containerPtr, _containerSize, index);
}

/*
 * Cubic Interpolation
 */
template<typename T>
T Interpolation<T>::cubicInterpolation(T *ptr, unsigned int size,  float index, bool catmullRom)
{
	if(_wrap)
		index = boundIndex(index, size);
	else if( !checkIndexWithinEdges(index, size, 4) )
		return index; // or return -1; or else

	// n
	int pIdx = (int)index;

	// n+1
	int nIdx = pIdx + 1;
	nIdx = boundIndex(nIdx, size);

	// n-1
	int ppIdx = pIdx - 1;
	ppIdx = boundIndex(ppIdx, size);

	// n+2
	int nnIdx = nIdx + 1;
	nnIdx = boundIndex(nnIdx, size);

	float frac = index - pIdx;
#if 0
	T ppVal = ptr[ppIdx];
	T pVal = ptr[pIdx];
	T nVal = ptr[nIdx];
	T nnVal = ptr[nnIdx];
	if(catmullRom)
	{
		T _c1 = 0.5 * (nVal - ppVal);
		T _c2 = 0.5 * (2 * ppVal - 5 * pVal + 4 * nVal - nnVal);
		T _c3 = 0.5 * (-ppVal + 3 * pVal - 3 * nVal + nnVal);
	}
	else
	{
		T _c1 = nVal - ppVal;
		T _c3 = nnVal - nVal - ppVal + pVal;
		T _c2 = ppVal - pVal - _c3;
	}
#endif
	T val[4] = {ptr[ppIdx], ptr[pIdx], ptr[nIdx], ptr[nnIdx]};

	T _c1, _c2, _c3;
	if(catmullRom)
	{
		_c1 = 0.5 * (val[2] - val[0]);
		_c2 = 0.5 * (2 * val[0] - 5 * val[1] + 4 * val[2] - val[3]);
		_c3 = 0.5 * (-val[0] + 3 * val[1] - 3 * val[2] + val[3]);
	}
	else
	{
		_c1 = val[2] - val[0];
		_c3 = val[3] - val[2] - val[0] + val[1];
		_c2 = val[0] - val[1] - _c3;
	}

	return frac * (_c1 + frac * (_c2 + frac * _c3)) + val[1];
}

template<typename T>
template<typename A>
T Interpolation<T>::cubicInterpolation(std::vector<T,A> const& vec, float index, bool catmullRom)
{
	return cubicInterpolation(vec.data(), vec.size(), index, catmullRom);
}

template<typename T>
template<size_t N>
T Interpolation<T>::cubicInterpolation(T (&ptr)[N], float index, bool catmullRom)
{
	return cubicInterpolation(&ptr, N*sizeof(T), index, catmullRom);
}

template<typename T>
T Interpolation<T>::cubicInterpolation(float index, bool catmullRom)
{
	if(_containerPtr == nullptr || _containerSize == 0)
		return index; // or return -1; or else
	else
		return cubicInterpolation(_containerPtr, _containerSize, index, catmullRom);
}

/*
 * Hermite Interpolation
 */
template<typename T>
T Interpolation<T>::hermiteInterpolation(T *ptr, unsigned int size,  float index, T tension, T bias)
{
	if(_wrap)
		index = boundIndex(index, size);
	else if( !checkIndexWithinEdges(index, size, 4) )
		return index; // or return -1; or else

	// n
	int pIdx = (int)index;

	// n+1
	int nIdx = pIdx + 1;
	nIdx = boundIndex(nIdx, size);

	// n-1
	int ppIdx = pIdx - 1;
	ppIdx = boundIndex(ppIdx, size);

	// n+2
	int nnIdx = nIdx + 1;
	nnIdx = boundIndex(nnIdx, size);

	float frac = index - pIdx;

	T val[4] = {ptr[ppIdx], ptr[pIdx], ptr[nIdx], ptr[nnIdx]};

	T mult = 0.5 * (1 + bias) * (1 - tension);

	T m0  = (val[1] - val[0]) * mult + (val[2] - val[1]) * mult;
	T m1  = (val[2] - val[1]) * mult + (val[3] - val[2]) * mult;

	T h00 = frac * frac * ( frac * 2 - 3) + 1;
	T h10 = frac * ( frac * (frac - 2) + 1);
	T h01 = frac * frac * (3 - 2 * frac);
	T h11 = frac * frac * ( frac - 1);

	return h00 * val[1] + h10 * m0 + h01 * m1 + h11 * val[2];
}

template<typename T>
template<typename A>
T Interpolation<T>::hermiteInterpolation(std::vector<T,A> const& vec, float index, T tension, T bias)
{
	return hermiteInterpolation(vec.data(), vec.size(), index, tension, bias);
}

template<typename T>
template<size_t N>
T Interpolation<T>::hermiteInterpolation(T (&ptr)[N], float index, T tension, T bias)
{
	return hermiteInterpolation(&ptr, N*sizeof(T), index, tension, bias);
}

template<typename T>
T Interpolation<T>::hermiteInterpolation(float index, T tension, T bias)
{
	if(_containerPtr == nullptr || _containerSize == 0)
		return index; // or return -1; or else
	else
		return hermiteInterpolation( _containerPtr, _containerSize, index, tension, bias);
}

/*
 * Parabolic Interpolation
 */
template<typename T>
T Interpolation<T>::parabolicInterpolation(T *ptr, unsigned int size, float index)
{
	if(_wrap)
		index = boundIndex(index, size);
	else if( !checkIndexWithinEdges(index, size, 3) )
		return index; // or return -1; or else

	int pIdx = (int)index;

	int nIdx = pIdx + 1;
	nIdx = boundIndex(nIdx, size);

	int ppIdx = pIdx - 1;
	ppIdx = boundIndex(ppIdx, size);

	float frac = index - pIdx;

	T val[] = {ptr[ppIdx], ptr[pIdx],  ptr[nIdx]};

        T adjust= (val[2] - val[0]) / (2 * (2 * val[1] - val[2] - val[0]));

        if (fabs(adjust) > 1)
		adjust = 0;

        return frac + adjust;
}

template<typename T>
template<typename A>
T Interpolation<T>::parabolicInterpolation(std::vector<T,A> const& vec, float index)
{
	return parabolicInterpolation(vec.data(), vec.size(), index);
}

template<typename T>
template<size_t N>
T Interpolation<T>::parabolicInterpolation(T (&ptr)[N], float index)
{
	return parabolicInterpolation(&ptr, N*sizeof(T), index);
}

template<typename T>
T Interpolation<T>::parabolicInterpolation(float index)
{
	if(_containerPtr == nullptr || _containerSize == 0)
		return index; // or return -1; or else
	else
		return parabolicInterpolation( _containerPtr, _containerSize, index);
}

template class Interpolation<double>;
template class Interpolation<float>;
#endif