/*
  This file is modified from the Arduino_LSM9DS1 library. (no magnetometer functions)
*/

#include "Bela_LSM9DS1.h"

#define LSM9DS1_ADDRESS            0x6b

#define LSM9DS1_WHO_AM_I           0x0f
#define LSM9DS1_CTRL_REG1_G        0x10
#define LSM9DS1_STATUS_REG         0x17
#define LSM9DS1_OUT_X_G            0x18
#define LSM9DS1_CTRL_REG6_XL       0x20
#define LSM9DS1_CTRL_REG8          0x22
#define LSM9DS1_OUT_X_XL           0x28

#define COMMAND_SLEEP_TIME_US      10000

LSM9DS1::LSM9DS1() :
  I2c(), continuousMode(false)
{
}

LSM9DS1::~LSM9DS1()
{
}

int LSM9DS1::setup() {
	// Set up I2C
	if(initI2C_RW(1, LSM9DS1_ADDRESS, -1)) {
		fprintf(stderr, "Unable to initialise I2C communication\n");
		return 1;
	}
	// reset IMU
	if (!writeRegister(LSM9DS1_CTRL_REG8, 0x05)) return -1;
	// Note: OG library resets magnetometer here but we're skipping since it isn't used

	usleep(COMMAND_SLEEP_TIME_US); // need to give enough time to process command

	if (readRegister(LSM9DS1_WHO_AM_I) != 0x68) {
    	close();
    	return 0;
	}

	writeRegister(LSM9DS1_CTRL_REG1_G, 0x78); // 119 Hz, 2000 dps, 16 Hz BW
	writeRegister(LSM9DS1_CTRL_REG6_XL, 0x70); // 119 Hz, 4g
	
	return 0;
}

void LSM9DS1::setContinuousMode() {
  // Enable FIFO (see docs https://www.st.com/resource/en/datasheet/DM00103319.pdf)
  writeRegister(0x23, 0x02);
  // Set continuous mode
  writeRegister(0x2E, 0xC0);

  continuousMode = true;
}

void LSM9DS1::setOneShotMode() {
  // Disable FIFO (see docs https://www.st.com/resource/en/datasheet/DM00103319.pdf)
  writeRegister(0x23, 0x00);
  // Disable continuous mode
  writeRegister(0x2E, 0x00);

  continuousMode = false;
}

void LSM9DS1::close()
{
	writeRegister(LSM9DS1_CTRL_REG1_G, 0x00);
	writeRegister(LSM9DS1_CTRL_REG6_XL, 0x00);
	closeI2C();
}

int LSM9DS1::readAcceleration(float& x, float& y, float& z)
{
  uint8_t data[3];

  if (!readRegisters(LSM9DS1_OUT_X_XL, (uint8_t*)data, sizeof(data))) {
    x = 0.0f;
    y = 0.0f;
    z = 0.0f;

    return 0;
  }

  x = data[0] * 4.0 / 32768.0;
  y = data[1] * 4.0 / 32768.0;
  z = data[2] * 4.0 / 32768.0;

  return 1;
}

int LSM9DS1::accelerationAvailable()
{
  if (continuousMode) {
    // Read FIFO_SRC. If any of the rightmost 8 bits have a value, there is data.
    if (readRegister(0x2F) & 63) {
      return 1;
    }
  } else {
    if (readRegister(LSM9DS1_STATUS_REG) & 0x01) {
      return 1;
    }
  }

  return 0;
}

float LSM9DS1::accelerationSampleRate()
{
  return 119.0F;
}

int LSM9DS1::readGyroscope(float& x, float& y, float& z)
{
  int16_t data[3];

  if (!readRegisters(LSM9DS1_OUT_X_G, (uint8_t*)data, sizeof(data))) {
    x = 0.0f;
    y = 0.0f;
    z = 0.0f;

    return 0;
  }

  x = data[0] * 2000.0 / 32768.0;
  y = data[1] * 2000.0 / 32768.0;
  z = data[2] * 2000.0 / 32768.0;

  return 1;
}

int LSM9DS1::gyroscopeAvailable()
{
  if (readRegister(LSM9DS1_STATUS_REG) & 0x02) {
    return 1;
  }

  return 0;
}

float LSM9DS1::gyroscopeSampleRate()
{
  return 119.0F;
}

int LSM9DS1::readRegister(uint8_t address)
{
	char buf[1] = { address };
	if (writeBytes(buf, (ssize_t )1) != 1) return -1;
	
	usleep(COMMAND_SLEEP_TIME_US); // need to give enough time to process command

	uint8_t data;
	readBytes(&data, 1);
	return data;
}

int LSM9DS1::readRegisters(uint8_t address, uint8_t* data, size_t length)
{
	uint8_t buf[1] = { (uint8_t)(0x80 | address) };
	if (writeBytes(buf, 1) != 1) return 0;
	
	usleep(COMMAND_SLEEP_TIME_US); // need to give enough time to process command

	readBytes(data, length);

	return 1;
}

int LSM9DS1::writeRegister(uint8_t address, uint8_t value)
{
	char buf[2] = { address, value };
	if (writeBytes(buf, 2) != 2) return 0;
	
	usleep(COMMAND_SLEEP_TIME_US); // need to give enough time to process command
	
	return 1;
}
