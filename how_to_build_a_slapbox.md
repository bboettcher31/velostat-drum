
# How to build a Slapbox
Please read through the [Slapbox publication](https://nime.pubpub.org/pub/ehocvk6o/release/1) first. When building everything, feel free to do things differently if you find better methods. Iteration is one of the project's main themes!

## The enclosure
A Slapbox enclosure is made by 3D printing the box frame and laser cutting its 6 sides from 6mm acrylic. Files for both tasks can be found in the fab_ folders in this repository.

### 3D printing
The 3D printed frame is pretty large, so you'll need to use a big printer (like the one at CIRMMT). For the guiro spikes and back panel conductors, you'll need to use conductive PLA. You'll also need to print the LED holders for the top panel.

### Laser cutting
In order to laser cut anything, you'll have to recolor the lines in the .dxf files (which tells the laser cutter what to cut/engrave) using a program that can open them. If you choose [CorelDraw](https://www.coreldraw.com/en/), there are corresponding .cdr files in the laser cutting folder you can use to avoid this step. If you see different versions of a print or cut file, be sure to choose the newest one.
Laser cutting 6mm acrylic:
-   Cut: 100 power, .2 speed, 2000Hz, 1 pass
-   Engrave: 100 power, 7.5 speed, 500Hz, 1 pass
-   Air assist: On

Laser cutting the cork coverings (all else defaults):
-   55 power for engraving
-   7 speed for cutting

## Sensor construction
As you read in the Slapbox paper, there are a variety of velostat pressure and position-sensing pads on the instrument. Use Figures 3 and 4 form the paper for reference to create concentric copper rings on the acrylic sides, and connect them to the underside of the panel using its holes. Once the copper is applied, cut and lay a layer of velostat in roughly the same shape over the top. The velostat should go a bit over the edge of the outermost copper strip to avoid shorts between the top and bottom copper layers. Next, create a solid top conductor using more copper tape (I taped it to paper and trimmed the shape I wanted), and place it face down on to the velostat, threading its connection through the final hole in the panel to attach to the back. Finally, use regular tape on the top conductor to firmly attach the layers to the panel. Repeat this process for the 2 small sides and the 2 top pads.

The pads on the back panel are a bit simpler, and you can 3D print the top and bottom conductors with conductive PLA. Insert a square of velostat between them (a bit larger to prevent shorts) and you'll be done with the pads! You can test their resistances changing with pressure using a multimeter. Finally, 3D print the back panel's 11 guiro spikes with conductive PLA and insert them into the panel.

Now that all of the connections from the sensors are on the inside of the panels, attach male-to-female jumper wires to each contact using a soldering iron (with plenty of flux). Wires can be attached to the guiro spikes by pressing the tip of a jumper wire into the contact and holding the soldering iron against the wire as it melts into the plastic (just remember to hold your breath). 

## Electronics
### Multiplexing inputs
As this project has a large number of analog inputs (18 from pads, and the volume knob), we need to [multiplex](https://sensorwiki.org/tutorials/multiplexing_inputs_to_a_microcontroller) them in order to be able to read them all from the Bela board. I used three [Pryth](https://prynth.github.io/) multiplexing boards, but feel free to make your own version with [8:1 multiplexing chips](https://www.mouser.ca/datasheet/2/308/1/mc14051b_d-1192948.pdf). Pay attention to which mux input you plug each sensor pin into, and refer to the `TouchPad` constructor and the corresponding instantiations in `render.cpp` to learn how to set the correct mux inputs to their pad inputs. Create the matching `muxPins` parameter for each pad in this fashion.
### Front panel and battery
Use a [battery charger chip](https://www.digikey.ca/en/products/detail/seeed-technology-co.,-ltd/106990290/10451861?utm_adgroup=Evaluation%20and%20Demonstration%20Boards%20and%20Kits&utm_source=google&utm_medium=cpc&utm_campaign=Shopping_Product_Development%20Boards%2C%20Kits%2C%20Programmers&utm_term=&productid=10451861&gclid=Cj0KCQjworiXBhDJARIsAMuzAuyr6s4rFkqzsJOY9GvPC--AGSdnDEWkKbHDihYqiszN9Y1T-BxNd_YaAlExEALw_wcB) with a 5V LIPO battery, and wire it to the front panel and Bela 5V input. The battery charger should have an EN (enable) pin, and connect that to a power switch on the front panel. Insert a potentiometer into the "phones" input on the front panel and connect it to the pin marked by the `PIN_VOLUME` constant in `render.cpp`. Finish up the front panel with a USB port in the "program" slot, a 1/4" audio jack connected to the Bela's audio output, and 2 speakers connected to the Bela's speaker left/right outputs.

### Guiro inputs
The pins coming from the guiro spikes should be connected to a [Trill Craft](https://shop.bela.io/products/trill-craft) board to manage capacitive touch readings, and connected to the I2C-1 inputs on the Bela.

### Protoboard construction
Next, make a breadboard/protoboard for connecting the pins from everything to the Bela. Use the `PIN_*` constants in `render.cpp` to see where to connect each pin to the Bela. I recommend testing things thoroughly with a breadboard and the Bela before moving to a protoboard.

## Did I miss anything important?
Let me know at bradyboettcher@gmail.com :)