/***** Centroid.h *****/
#pragma once

#include <type_traits>
#include <cstddef>
#include <vector>

namespace Centroid {
	static inline float getCentroid(float* values, int size) {
		// Get index with max value
		float sum = 0.0f;
		for (int i = 0; i < size; ++i) {
			sum += values[i];
		}
		if (sum == 0.0f) return 0.0f;
		// Make centroid sum from normalized and weighted values
		float centroid = 0.0f;
		for (int i = 0; i < size; ++i) {
			centroid += i * (values[i] / sum);
		}
		return centroid;
	}
}