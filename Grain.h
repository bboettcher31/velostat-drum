/*
  ==============================================================================

    Grain.h
    Created: 11 Apr 2021 8:51:46pm
    Author:  brady

  ==============================================================================
*/

#pragma once
#include <Bela.h>
#include <vector>
#include "Grain.h"

class Grain {
 public:
  Grain(std::vector<float>* fileBuffer, int duration, float pbRate, int startPos, int trigTs, float gain)
      : fileBuffer(fileBuffer), duration(duration), pbRate(pbRate), startPos(startPos), trigTs(trigTs), gain(gain) {}
  ~Grain() {}

  float process(int ts);
  bool isExpired(); // True if grain has finished playing

  std::vector<float>* fileBuffer; // sample buffer to play
  int duration;  // Grain duration in samples
  float pbRate;  // Playback rate (1.0 being regular speed)
  int startPos;  // Start position in file to play from in samples
  int trigTs;    // Timestamp when grain was triggered in samples
  float gain;    // Grain gain

 private:
	int mCurTs;
};