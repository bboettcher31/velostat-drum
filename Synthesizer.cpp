/***** Synthesizer.cpp *****/
#include "Synthesizer.h"
#include <algorithm>

float Synthesizer::process(int ts) {
    float output = 0.0f;
    for (int g = 0; g < mGrains.size(); ++g) {
		output += mGrains[g].process(ts);
	}
    return output *= mGain;
}

void Synthesizer::tapDetected(BelaContext* context, int ts, TouchPad& pad, float playbackSpeed) {
	if (mGrains.size() < MAX_GRAINS) {
		//rt_printf("tap gain: %f, centroid: %f\n", pad.getPressure(), pad.getCentroid());
		float gain = constrain(map(pad.getPressure(), 0.0f, 0.5f, 0.5f, 1.0f), 0.5f, 1.0f);
		// Crossfade between pad samples based on centroid
		if (pad.sampleBuffers.size() > 1) {
			mGrains.push_back(Grain(&pad.sampleBuffers[0], pad.sampleBuffers[0].size(), playbackSpeed, 0,
    			ts, gain * pad.getCentroid()));
    		mGrains.push_back(Grain(&pad.sampleBuffers[1], pad.sampleBuffers[1].size(), playbackSpeed, 0,
    			ts, gain * (1.0f - pad.getCentroid())));
		} else {
			mGrains.push_back(Grain(&pad.sampleBuffers[0], pad.sampleBuffers[0].size(), playbackSpeed, 0, ts, gain));
		}
	}
}

void Synthesizer::guiroTapDetected(BelaContext* context, int ts, float guiroPosition, float playbackSpeed) {
	if (mGrains.size() < MAX_GRAINS) {
		float pbSpeed = (1.0f + guiroPosition) * playbackSpeed;
		mGrains.push_back(Grain(&mGuiroSample, mGuiroSample.size(), pbSpeed, 0, ts, 0.7f));
	}
}

void Synthesizer::checkRemoveGrains() {
	mGrains.erase(
    	std::remove_if(mGrains.begin(), mGrains.end(),
        	[](Grain & g) { return g.isExpired(); }),
    	mGrains.end());
}