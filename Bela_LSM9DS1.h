/*
  This file is modified from the Arduino_LSM9DS1 library.
*/

#include <I2c.h>
#include <stdint.h>

class LSM9DS1: public I2c {
  public:
    LSM9DS1();
    ~LSM9DS1();

    int setup();
    void close();

    // Controls whether a FIFO is continuously filled, or a single reading is stored.
    // Defaults to one-shot.
    void setContinuousMode();
    void setOneShotMode();

    // Accelerometer
    int readAcceleration(float& x, float& y, float& z); // Results are in g (earth gravity).
    int accelerationAvailable(); // Number of samples in the FIFO.
    float accelerationSampleRate(); // Sampling rate of the sensor.

    // Gyroscope
    int readGyroscope(float& x, float& y, float& z); // Results are in degrees/second.
    int gyroscopeAvailable(); // Number of samples in the FIFO.
    float gyroscopeSampleRate(); // Sampling rate of the sensor.
    
    int readI2C() { return 0; };

  private:
    bool continuousMode;
    int readRegister(uint8_t address);
    int readRegisters(uint8_t address, uint8_t* data, size_t length);
    int writeRegister(uint8_t address, uint8_t value);
};
