/***** Synthesizer.h *****/
#pragma once
#include "Grain.h"
#include <Bela.h>
#include "TouchPad.h"

class Synthesizer {
public:

  enum SynthMode {
    DRUM_SAMPLE = 0,
  };

  Synthesizer() {
  	mGuiroSample = AudioFileUtilities::loadMono(GUIRO_FILE);
  }
  ~Synthesizer() { }

  void setMode(SynthMode mode) { mMode = mode; }
  SynthMode getMode() { return mMode; }
  void setGain(float gain) { mGain = gain; }
  float process(int ts);
  void tapDetected(BelaContext* context, int ts, TouchPad& pad, float playbackSpeed);
  void guiroTapDetected(BelaContext* context, int ts, float guiroPosition, float playbackSpeed); // guiroPosition from 0-1 left to right
  void checkRemoveGrains();

 private:
  static constexpr auto MAX_GRAINS = 20;  // Max grains active at once
  static constexpr auto GUIRO_FILE = "c_click.wav";

  std::vector<float> mGuiroSample;
  SynthMode mMode = SynthMode::DRUM_SAMPLE;
  std::vector<Grain> mGrains;
  float mGain = 0.0f;
  
};