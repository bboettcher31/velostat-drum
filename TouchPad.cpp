/***** TouchPad.cpp *****/

#include "TouchPad.h"
#include <Bela.h>
#include <cstdlib>

TouchPad::TouchPad(std::vector<MuxConfig> muxPins, std::vector<std::string> sampleFiles, std::vector<int> ledPins, float threshold)
	: ledPins(ledPins), mMuxPins(muxPins), mImpulseDetector(threshold) {
	mInterpolator.setup(false);

	for (std::string &file : sampleFiles) {
		sampleBuffers.push_back(AudioFileUtilities::loadMono(file));
	}

	// Count total number of pins
	int numPins = 0;
	for (MuxConfig& mux: muxPins) {
		numPins += mux.channels.size();
	}
	mRawValues.resize(numPins, 0.0f);
	mNormValues.resize(numPins, 0.0f);
	mRestingValues.resize(numPins, 0.0f);
}

void TouchPad::calibrate() {
	for (int i = 0; i < mRawValues.size(); ++i) {
		mRestingValues[i] = mRawValues[i];
		
	}
	mImpulseDetector.reset();
}

bool TouchPad::checkPushValues(BelaContext* context, int frame, int ts) {
	int muxChannel = multiplexerChannelForFrame(context, frame);
	int curPins = 0;
	bool usedValue = false;
	for (MuxConfig& mux: mMuxPins) {
		for (int i = 0; i < mux.channels.size(); ++i) {
			if (mux.channels[i] == muxChannel) {
				// Read sample
				float input = analogRead(context, frame, mux.index);
				int valIdx = curPins + i;
				mRawValues[valIdx] = input;
				mNormValues[valIdx] = normalize(valIdx, input);
				usedValue = true;
				//if (mux.index == 0) rt_printf("mux: %d, pin: %d, val: %f\n", muxChannel, valIdx, input);
				break; // Stop searching this mux
			}
		}
		curPins += mux.channels.size();
	}
	if (!usedValue) return false;

	mCentroid = Centroid::getCentroid(mNormValues.data(), mNormValues.size());
	mPressure = mInterpolator.linearInterpolation(mNormValues.data(), mNormValues.size(), mCentroid);
	mCentroid = 1.0f - (mCentroid / (mNormValues.size() - 1)); // Normalize centroid
	mCentroid = constrain(mCentroid, 0.0f, 1.0f);
	bool isImpulse = mImpulseDetector.pushValue(mPressure, ts);
	return isImpulse;
}

float TouchPad::normalize(int idx, float input) {
	float x = constrain(input, mRestingValues[idx], mMaxValue);
	return (x - mRestingValues[idx]) / (mMaxValue - mRestingValues[idx]);
}