let PADDING_PERC = 0.02;
let MAIN_PANEL_HEIGHT_PERC = 0.4;
let TOP_WIDTH_PERC = 0.55;
let GUIRO_WIDTH_PERC = 0.6;
let GUIRO_HEIGHT_PERC = 0.15;
let HEXAGON_PERC = 0.13;
let BUTTON_PERC = 0.05;
let MAX_PRESSURE_STROKE = 10;
let CALIBRATION_FRAMES = 10;
let PRESSURE_THRESH = 0.1;

let fileLeftInput;

let fileRightInput;
let fileTop1Input;
let fileTop2Input;
let fileGuiroInput;
let btnCalibrate;
let shouldCalibrate = false;
let calibrateCounter = 0;

const BufferType = Object.freeze({
	CENTROIDS: 0,
	PRESSURES: 1,
	IMPULSES: 2,
    BUTTONS: 3,
    GUIRO: 4,
});

const ButtonType = Object.freeze({
	LEFT: 0,
	RIGHT: 1,
});

const PadType = Object.freeze({
	LEFT: 0,
	RIGHT: 1,
	BACK_LEFT: 2,
	BACK_RIGHT: 3,
	TOP_LEFT: 4,
	TOP_RIGHT: 5,
});

function setup() {
  createCanvas(windowWidth, windowHeight);
  
  // Setup callbacks for file loading
  fileLeftInput = createFileInput(handleFileLeft, false);
  fileRightInput = createFileInput(handleFileRight, false);
  fileTop1Input = createFileInput(handleFileTop1, false);
  fileTop2Input = createFileInput(handleFileTop2, false);
  btnCalibrate = createButton("Normalize");
  btnCalibrate.position(windowWidth / 2, 0);
  btnCalibrate.mousePressed(handleCalibrate);
}

function draw() {
  clear();
  background(220);
  
  drawPanels();
  //drawParameters();

  Bela.data.sendBuffer(0, 'int', shouldCalibrate);
  if (calibrateCounter >= CALIBRATION_FRAMES) shouldCalibrate = false;
  calibrateCounter++;
}

function drawPanels() {
  let padding = windowWidth * PADDING_PERC;
  rectMode(CENTER);
  let normalColor = color(0, 0, 0);
  let impulseColor = color(255, 0, 0);
  noFill();
  // Draw left side
  let smallWidth = windowWidth * (1 - TOP_WIDTH_PERC) / 2 - padding;
  let mainHeight = windowHeight * MAIN_PANEL_HEIGHT_PERC;
  stroke(Bela.data.buffers[BufferType.IMPULSES][PadType.LEFT] == 1 ? impulseColor : normalColor);
  strokeWeight(1 + Bela.data.buffers[BufferType.PRESSURES][PadType.LEFT] * MAX_PRESSURE_STROKE);
  rect(smallWidth / 2 + padding / 2, windowHeight / 2, smallWidth, mainHeight, 20);
  // Draw left centroid
  if (Bela.data.buffers[BufferType.PRESSURES][PadType.LEFT] > PRESSURE_THRESH) {
    let centroid = Bela.data.buffers[BufferType.CENTROIDS][PadType.LEFT];
    rect(smallWidth / 2 + padding / 2, windowHeight / 2, smallWidth * centroid, mainHeight * centroid, 20);
  }
  // Draw right panel
  stroke(Bela.data.buffers[BufferType.IMPULSES][PadType.RIGHT] == 1 ? impulseColor : normalColor);
  strokeWeight(1 + Bela.data.buffers[BufferType.PRESSURES][PadType.RIGHT] * MAX_PRESSURE_STROKE);
  rect(windowWidth - smallWidth / 2 - padding / 2, windowHeight / 2, smallWidth, mainHeight, 20);
  // Draw right centroid
  if (Bela.data.buffers[BufferType.PRESSURES][PadType.RIGHT] > PRESSURE_THRESH) {
    let centroid = Bela.data.buffers[BufferType.CENTROIDS][PadType.RIGHT];
    rect(windowWidth - smallWidth / 2 - padding / 2, windowHeight / 2, smallWidth * centroid, mainHeight * centroid, 20);
  }
  // Draw top panel
  stroke(normalColor);
  strokeWeight(2);
  let topWidth = windowWidth * TOP_WIDTH_PERC - padding;
  rect(windowWidth / 2, windowHeight / 2, topWidth, mainHeight, 20);
  // Draw hexagons
  stroke(Bela.data.buffers[BufferType.IMPULSES][PadType.TOP_LEFT] == 1 ? impulseColor : normalColor);
  strokeWeight(1 + Bela.data.buffers[BufferType.PRESSURES][PadType.TOP_LEFT] * MAX_PRESSURE_STROKE);
  polygon(windowWidth / 2 - topWidth / 4, windowHeight / 2, windowHeight * HEXAGON_PERC, 6);
  // Left hexagon centroid
  if (Bela.data.buffers[BufferType.PRESSURES][PadType.TOP_LEFT] > PRESSURE_THRESH) {
    let centroid = 1.0 - Bela.data.buffers[BufferType.CENTROIDS][PadType.TOP_LEFT];
    polygon(windowWidth / 2 - topWidth / 4, windowHeight / 2, windowHeight * HEXAGON_PERC * centroid, 6);
  }
  stroke(Bela.data.buffers[BufferType.IMPULSES][PadType.TOP_RIGHT] == 1 ? impulseColor : normalColor);
  strokeWeight(1 + Bela.data.buffers[BufferType.PRESSURES][PadType.TOP_RIGHT] * MAX_PRESSURE_STROKE);
  polygon(windowWidth / 2 + topWidth / 4, windowHeight / 2, windowHeight * HEXAGON_PERC, 6);
  // Right hexagon centroid
  if (Bela.data.buffers[BufferType.PRESSURES][PadType.TOP_RIGHT] > PRESSURE_THRESH) {
    let centroid = 1.0 - Bela.data.buffers[BufferType.CENTROIDS][PadType.TOP_RIGHT];
    polygon(windowWidth / 2 + topWidth / 4, windowHeight / 2, windowHeight * HEXAGON_PERC * centroid, 6);
  }
  // Draw buttons
  if (Bela.data.buffers[BufferType.BUTTONS][ButtonType.LEFT] == 1) {
  	fill(normalColor);
  } else {
  	stroke(normalColor);
  	strokeWeight(2);
  }
  ellipse(windowWidth / 2 - topWidth / 2 + padding, windowHeight / 2 + mainHeight / 2 - padding, windowHeight * BUTTON_PERC);
  noFill();
  if (Bela.data.buffers[BufferType.BUTTONS][ButtonType.RIGHT] == 1) {
  	fill(normalColor);
  } else {
  	stroke(normalColor);
  	strokeWeight(2);
  }
  ellipse(windowWidth / 2 + topWidth / 2 - padding, windowHeight / 2 + mainHeight / 2 - padding, windowHeight * BUTTON_PERC);
  // Draw guiro panel
  noFill();
  let guiroHeight = windowHeight * GUIRO_HEIGHT_PERC;
  let guiroY = windowHeight / 2 + mainHeight / 2 + padding + guiroHeight / 2;
  stroke(normalColor);
  strokeWeight(2);
  rect(windowWidth / 2, guiroY, topWidth, guiroHeight, 20);
  // Draw guiro
  let guiroWidth = topWidth * GUIRO_WIDTH_PERC - padding;
  stroke(normalColor);
  strokeWeight(2);
  rect(windowWidth / 2,  guiroY, guiroWidth, guiroHeight - padding);
  // Guiro touches
  for (let i = 0; i < 11; i++) {
  	if (Bela.data.buffers[BufferType.GUIRO][i] > 0.07) {
  		let centroid = (i / 11.0) + 1/24;
    	rect(windowWidth / 2 - guiroWidth / 2 + centroid * guiroWidth,  guiroY, 5, guiroHeight - padding);
  	}
  }
  // Draw back left side pad
  let backPadWidth = (topWidth - guiroWidth) / 2 - padding;
  stroke(Bela.data.buffers[BufferType.IMPULSES][PadType.BACK_LEFT] == 1 ? impulseColor : normalColor);
  strokeWeight(1 + Bela.data.buffers[BufferType.PRESSURES][PadType.BACK_LEFT] * MAX_PRESSURE_STROKE);
  rect(windowWidth / 2 - topWidth / 2 + padding / 2 + backPadWidth / 2, guiroY, backPadWidth, guiroHeight - padding);
  // Draw back right side pad
  stroke(Bela.data.buffers[BufferType.IMPULSES][PadType.BACK_RIGHT] == 1 ? impulseColor : normalColor);
  strokeWeight(1 + Bela.data.buffers[BufferType.PRESSURES][PadType.BACK_RIGHT] * MAX_PRESSURE_STROKE);
  rect(windowWidth / 2 + topWidth / 2 - padding / 2 - backPadWidth / 2, guiroY, backPadWidth, guiroHeight - padding);
}

function polygon(x, y, radius, npoints) {
  let angle = TWO_PI / npoints;
  beginShape();
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius;
    let sy = y + sin(a) * radius;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}

function drawParameters() {
  let padding = windowWidth * PADDING_PERC;
  let mainHeight = windowHeight * MAIN_PANEL_HEIGHT_PERC;
  let y = windowHeight / 2 - mainHeight / 2 - 25;
  // Draw top file inputs
  fileTop1Input.position(windowWidth / 2 - windowWidth * TOP_WIDTH_PERC / 2 + padding * 2, y);
  fileTop1Input.size(170, 25);
  fileTop2Input.position(windowWidth / 2 + padding, y);
  fileTop2Input.size(170, 25);
  // Draw left file input
  y += mainHeight + padding * 2;
  fileLeftInput.position(padding, y);
  fileLeftInput.size(170, 25);
  // Draw right file input
  let topWidth = windowWidth / 2 + windowWidth * TOP_WIDTH_PERC / 2;
  fileRightInput.position(padding + topWidth, y);
  fileRightInput.size(170, 25);
}

function handleCalibrate() {
  shouldCalibrate = true;
  calibrateCounter = 0;
}

function handleFileLeft(file) {
  if (file.type != 'audio') return;
  print(file.name);
}
function handleFileRight(file) {
  if (file.type != 'audio') return;
  print(file.name);
}
function handleFileTop1(file) {
  if (file.type != 'audio') return;
  print(file.name);
}
function handleFileTop2(file) {
  if (file.type != 'audio') return;
  print(file.name);
}
