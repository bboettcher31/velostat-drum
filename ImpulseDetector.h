/***** ImpulseDetector.h *****/
#pragma once

/* Performs impulse detection using pseudo-derivatives and peak detection

Algorithm:
- Calculate pressure delta, acceleration and jerk over time
- If jerk goes above the threshold and then decreases, trigger impulse
- Once pressure value goes below release threshold, start looking again

*/
class ImpulseDetector {
public:
	ImpulseDetector(float threshold): mThreshold(threshold), mPeakVal(0.0f), mPeakJerk(0.0f) {}
	~ImpulseDetector() {}
	
	void setSampleRate(float sampleRate) { mSampleRate = sampleRate; }
	
	// Returns true if value triggered an impulse
	bool pushValue(float newValue, int ts);
	
	void reset() { 
		mPeakJerk = 0.0f;
		mPeakVal = 0.0f;
		mImpulseStartTs = NO_IMPULSE_PLAYING;
		mImpulseState = ImpulseState::LOOKING;
	}
	
	bool  isTriggered() { return mImpulseState == ImpulseState::AFTERSHOCK; }
	int   getHoldTs() { return mHoldTs; }
	float getValue() { return mVal; }
	float getDelta() { return mDelta; }
	float getJerk()  { return mJerk; }
	void  resetHoldTs(float ts) { mHoldTs = ts; }
	
private:
	enum ImpulseState {
		LOOKING,
		PEAK,
		AFTERSHOCK
	};
	
	static constexpr auto AFTERSHOCK_MS = 20;
	//static constexpr auto MIN_IMPULSE_LEN_MS = 12;
	static constexpr auto RELEASE_THRESHOLD = 0.05f;
	static constexpr auto NO_IMPULSE_PLAYING = -1;

	int mImpulseStartTs = NO_IMPULSE_PLAYING;
	float mThreshold;
	float mSampleRate;
	float mPeakJerk, mPeakVal;
	ImpulseState mImpulseState = ImpulseState::LOOKING;
	int mHoldTs = 0; // Stops updating when holding after a tap
	float mVal, mPrevVal, mDelta, mPrevDelta, mJerk;
};