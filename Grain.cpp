/*
  ==============================================================================

    Grain.cpp
    Created: 11 Apr 2021 8:51:46pm
    Author:  brady

  ==============================================================================
*/

#include "Grain.h"
#include <cmath>

float Grain::process(int ts) {
  mCurTs = ts;
  if (startPos < 0 || fileBuffer == nullptr) return 0.0f;
  //float timePerc = (ts - trigTs) / (float)duration;
  //if (timePerc <= 0.0f || timePerc >= 1.0f) return 0.0f;
  
  int curIdx = (mCurTs - trigTs) * pbRate;
  if (curIdx > duration) return 0.0f;
  float sample = fileBuffer->at(curIdx % fileBuffer->size());
  
/*
  float unStretchedDuration = duration * pbRate;
  int lowSample = std::floor(max(0.0f, timePerc * unStretchedDuration));
  int highSample = std::ceil(max(0.0f, timePerc * unStretchedDuration));
  if (highSample >= fileBuffer->size()) return 0.0f;
  float rem = (timePerc * unStretchedDuration) - lowSample;

  float sample = map(rem, 0.0f, 1.0f, fileBuffer->at((startPos + lowSample) % fileBuffer->size()),
                              fileBuffer->at((startPos + highSample) % fileBuffer->size()));
*/
  sample *= gain;
  return sample;
}

bool Grain::isExpired() {
  return mCurTs - trigTs > duration;
}