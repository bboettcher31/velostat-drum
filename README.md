
# Slapbox: A fully standalone digital percussion instrument on the Bela platform

## Pushing changes from Bela

```
git pull root@bela.local:Bela/projects/velostat-drum master

git push origin master
```

## Pulling remote changes to Bela

```
git pull origin master
git push root@bela.local:Bela/projects/velostat-drum master:tmp
ssh root@bela.local
cd Bela/projects/velostat-drum
git merge tmp
git branch -D tmp
```
