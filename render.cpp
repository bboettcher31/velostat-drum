#include <Bela.h>
#include "Bela_LSM9DS1.h"
#include <libraries/Gui/Gui.h>
#include <libraries/Trill/Trill.h>
#include <libraries/Debounce/BelaDebounce.h>
#include <libraries/OnePole/OnePole.h>
#include "TouchPad.h"
#include "Synthesizer.h"

static constexpr auto GUI_REFRESH_RATE = 0.08f; // Secs per update
static constexpr auto BUTTON_DEBOUNCE_MS = 5;
static constexpr auto PIN_LEFT_BUTTON = 7;
static constexpr auto PIN_RIGHT_BUTTON = 6;
static constexpr auto PIN_VOLUME = 4;
static constexpr auto NUM_GUIRO_PINS = 11;
static constexpr auto GUIRO_TOUCH_THRESHOLD = 0.07f;
static constexpr int GUIRO_LED_PINS[2] = { 8, 9 };
static constexpr auto RETRIGGER_MS = 234.0f; // Hold time before samples retrigger

enum PadType {
	LEFT = 0,
	RIGHT,
	BACK_LEFT,
	BACK_RIGHT,
	TOP_LEFT,
	TOP_RIGHT,
	NUM_PADS
};

// Velostat pads (in order of PadType)
std::vector<TouchPad> mPads = {
	TouchPad({{1, {2, 3, 4}}}, {"kick1.wav", "kick2.wav"}, {3}), // Left side
	TouchPad({{1, {5, 6, 7}}}, {"hat1.wav", "hat2.wav"}, {4}), // Right side
	TouchPad({{2, {0}}}, {"g_minor_chord.wav"}, {1}, 0.07f), // Left back side
	TouchPad({{3, {0}}}, {"c_minor_chord.wav"}, {0}, 0.07f), // Right back side
	TouchPad({ {0, {5, 6, 7}}, {1, {0, 1}}}, {"snare1.wav", "snare2.wav"}, {2}), // Top left
	TouchPad({{0, {0, 1, 2, 3, 4}}}, {"shaker1.wav", "shaker2.wav"}, {5}), // Top right
};

// Bookkeeping
int mTotalSamps;
int mSamplesPerGuiUpdate, mSamplesPerRetrigger;
BelaDebounce mLeftButtonDb, mRightButtonDb;
Trill mGuiro; // Trill craft with guiro spikes attached
float mGuiroValues[NUM_GUIRO_PINS] = { 0.0 };
bool mActiveGuiroSpikes[NUM_GUIRO_PINS] = { false };
bool mLeftButtonPressed, mRightButtonPressed;
Synthesizer mSynth;

// GUI
/* 
	Data Legend:
	0: Vector of centroids in order of PadType
	1: Vector of pressures in order of PadType
	2: Vector of impulses in order of PadType
	3: Vector of button presses {left, right}
*/
Gui mGui;
float mCentroids[PadType::NUM_PADS];
float mPressures[PadType::NUM_PADS];
int mImpulses[PadType::NUM_PADS]; // Int cus Bela has some boolean probs

// IMU
LSM9DS1 mImu;
float mAccelData[3];

void calibratePads();

void loop(void*)
{
	while(!Bela_stopRequested())
	{
		// Read raw data from sensor
		mGuiro.readI2C();
		for(unsigned int i = 0; i < NUM_GUIRO_PINS; i++)
			mGuiroValues[i] = mGuiro.rawData[i];
		usleep(50000);
	}
}

bool setup(BelaContext *context, void *userData)
{
	// Setup IMU
	if (int res = mImu.setup()) {
		rt_printf("Error setting up IMU: %d\n", res);
	} else {
		rt_printf("IMU ready for use\n");
	}
	mImu.setOneShotMode();
	mAccelData[0] = 0.0f;
	mAccelData[1] = 0.0f;
	mAccelData[2] = 0.0f;
	
	for (TouchPad& pad : mPads) {
		// Init sensor reading params
		pad.setSampleRate(context->audioSampleRate);
		// LED pin modes
		for (int& pin : pad.ledPins) {
			pinMode(context, 0, pin, OUTPUT);
		}
	}
	mPads[PadType::BACK_LEFT].setMaxValue(0.63f);
	mPads[PadType::BACK_RIGHT].setMaxValue(0.63f);
	
	// Setup a Trill Craft on i2c bus 1, using the default address for the guiro
	if(mGuiro.setup(1, Trill::CRAFT) != 0) {
		fprintf(stderr, "Unable to initialise Trill Craft\n");
		return false;
	}
	for (const int& pin: GUIRO_LED_PINS) {
		pinMode(context, 0, pin, OUTPUT);
	}
	// Set and schedule auxiliary task for reading guiro sensor data from the I2C bus
	Bela_runAuxiliaryTask(loop);
	
	// Setup bookkeeping
	mTotalSamps = 0;
	mSamplesPerGuiUpdate = context->audioSampleRate * GUI_REFRESH_RATE;
	mSamplesPerRetrigger = context->audioSampleRate * (RETRIGGER_MS / 1000.0f);
	mLeftButtonDb = BelaDebounce({context, PIN_LEFT_BUTTON, BUTTON_DEBOUNCE_MS});
	mRightButtonDb = BelaDebounce({context, PIN_RIGHT_BUTTON, BUTTON_DEBOUNCE_MS});
	mLeftButtonPressed = mRightButtonPressed = false;
	
	// Setup gui
	mGui.setup(context->projectName);
	mGui.setBuffer('d', 1);
	
	return true;
}

void render(BelaContext *context, void *userData)
{
	// Update IMU data
	if (mImu.accelerationAvailable()) {
		if (int res = mImu.readAcceleration(mAccelData[0], mAccelData[1], mAccelData[2])) {
			
		} else {
			//rt_printf("Error reading acceleration: %d\n", res);
		}
	}

	// Check for calibration trigger
	DataBuffer& buffer = mGui.getDataBuffer(0);
	int* shouldCalibrate = buffer.getAsInt();
	bool startupCalibrate = mTotalSamps > 2000 && mTotalSamps < 2500;
	if (shouldCalibrate[0] == 1 || startupCalibrate) {
		calibratePads();
		rt_printf("Calibrating pads\n");
	}
	
	// Detect button presses
	// If either button pressed, activate delay modulation
	mLeftButtonPressed = mLeftButtonDb.process(context);
	mRightButtonPressed = mRightButtonDb.process(context);
	float playbackSpeed = mLeftButtonPressed ? 0.5f : 
										(mRightButtonPressed ? 2.0f : 1.0f);

	// Sample analog data if needed
	// TODO: put inside audio loop by computing samples per read to sync with total samps
	for(unsigned int n = 0; n < context->analogFrames; n++) {
		for (int i = 0; i < mPads.size(); ++i) {
			if (mPads[i].checkPushValues(context, n, mTotalSamps)) {
				// Impulse detected
				mSynth.tapDetected(context, mTotalSamps, mPads[i], playbackSpeed);
			} else if (mTotalSamps - mPads[i].getHoldTs() > mSamplesPerRetrigger) {
				mPads[i].resetHoldTs(mTotalSamps);
				mSynth.tapDetected(context, mTotalSamps, mPads[i], playbackSpeed);
			}
		}
		// Update synth volume from knob
		if (multiplexerChannelForFrame(context, n) == PIN_VOLUME) {
			mSynth.setGain(1.0f - analogRead(context, n, PIN_VOLUME));
		}
	}
	
	// Write LED pins based on impulses
	for (TouchPad& pad: mPads) {
		for (int& pin : pad.ledPins) {
			pinMode(context, 0, pin, OUTPUT);
			digitalWrite(context, 0, pin, pad.getTriggered());
		}
	}
	
	// Calculate guiro interactions
	bool leftLedActive = false;
	bool rightLedActive = false;
	for(unsigned int i = 0; i < NUM_GUIRO_PINS; i++) {
		if (mGuiroValues[i] >= GUIRO_TOUCH_THRESHOLD) {
			if (!mActiveGuiroSpikes[i]) {
				mSynth.guiroTapDetected(context, mTotalSamps, i/((float)NUM_GUIRO_PINS-1), playbackSpeed);
				mActiveGuiroSpikes[i] = true;
				if (i < NUM_GUIRO_PINS / 2.0f) leftLedActive = true;
				else rightLedActive = true;
			}
		} else {
			mActiveGuiroSpikes[i] = false;
		}
	}
	// Activate guiro LEDs
	pinMode(context, 0, GUIRO_LED_PINS[0], OUTPUT);
	digitalWrite(context, 0, GUIRO_LED_PINS[0], leftLedActive);
	pinMode(context, 0, GUIRO_LED_PINS[1], OUTPUT);
	digitalWrite(context, 0, GUIRO_LED_PINS[1], rightLedActive);
	
	for (int i = 0; i < context->audioFrames; ++i) {
		// Process audio
		float output = 0.0f;
		output += mSynth.process(mTotalSamps);		
		for(unsigned int channel = 0; channel < context->audioOutChannels; channel++) {
			audioWrite(context, i, channel, output);
		}
		
		// Update GUI if necessary
		if (!(mTotalSamps % mSamplesPerGuiUpdate))
		{
			/*for(unsigned int i = 0; i < NUM_GUIRO_PINS; i++) {
				rt_printf("#%d: %f, ", i, mGuiroValues[i]);
			}*/
			//rt_printf("%d\n", mSynth.getNumGrains());
			for (int i = 0; i < mPads.size(); ++i) {
				mCentroids[i] = mPads[i].getCentroid();
				mPressures[i] = mPads[i].getPressure();
				mImpulses[i] = mPads[i].getTriggered() ? 1 : 0;
			}
			mGui.sendBuffer(0, mCentroids); // Centroid touch position relative to center
			mGui.sendBuffer(1, mPressures); // Pressure from centroid interpolation
			mGui.sendBuffer(2, mImpulses); // Impulse trigger status
			std::vector<int> buttons = {mLeftButtonPressed, mRightButtonPressed};
			mGui.sendBuffer(3, buttons);
			mGui.sendBuffer(4, mGuiroValues);
		}

		mTotalSamps++;
	}
	mSynth.checkRemoveGrains();
}

void cleanup(BelaContext *context, void *userData)
{
	mImu.close();
}

void calibratePads() {
	for (TouchPad& pad: mPads) {
		pad.calibrate();
	}
}