/***** ImpulseDetector.cpp *****/
#include "ImpulseDetector.h"
#include <cmath>
#include <Bela.h>

bool ImpulseDetector::pushValue(float newValue, int ts) {
	mVal = newValue;
	mDelta = (mPrevVal - mVal);
	mJerk = fabs(mPrevDelta - mDelta);
	
	mPrevVal = mVal;
	mPrevDelta = mDelta;
	
	switch (mImpulseState) {
    case ImpulseState::LOOKING: {
    	mHoldTs = ts;
    	if (mJerk > mThreshold && mDelta > 0.0f && mVal > RELEASE_THRESHOLD) {
        	mPeakJerk = mJerk;
        	mImpulseState = ImpulseState::PEAK;
    	}
    	break;
    }
    case ImpulseState::PEAK: {
    	mHoldTs = ts;
    	if (mJerk > mPeakJerk) {
        	// Keep climbing
        	mPeakJerk = mJerk;
			mPeakVal = mVal;
    	} else {
        	// Impulse detected
        	mImpulseStartTs = ts;
        	mImpulseState = ImpulseState::AFTERSHOCK;
        	return true;
    	}
    	break;
    }
    case ImpulseState::AFTERSHOCK: {
    	int impulseTimeMs = (((float)ts - mImpulseStartTs) / mSampleRate) * 1000.0f;
    	//rt_printf("start %d, ts: %d\n", mImpulseStartTs, ts);
    	if (mVal < RELEASE_THRESHOLD && impulseTimeMs >= AFTERSHOCK_MS) {
        	mImpulseState = ImpulseState::LOOKING;
			mPeakVal = 0.0f;
			mPeakJerk = 0.0f;
			mImpulseStartTs = NO_IMPULSE_PLAYING;
    	}
    	break;
    }
    default: {}
	}
	
	return false;
}