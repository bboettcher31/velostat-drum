/***** TouchPad.h *****/
#pragma once

#include <Bela.h>
#include "Centroid.h"
#include "ImpulseDetector.h"
#include "Interpolation.h"
#include <libraries/AudioFile/AudioFile.h>

class TouchPad {
public:

	typedef struct MuxConfig {
		int index; // Analog channel index
		std::vector<int> channels; // Mux channels
	} MuxConfig;

	TouchPad(std::vector<MuxConfig> muxPins, std::vector<std::string> sampleFiles, std::vector<int> ledPins, float threshold = TAP_THRESHOLD);
	~TouchPad() {}
	
	void calibrate(); // Normalizes pressure values for resting readings
	bool checkPushValues(BelaContext* context, int frame, int ts); // Returns true if impulse was detected
	void setSampleRate(float sampleRate) {
		mImpulseDetector.setSampleRate(sampleRate);
	}
	void setMaxValue(float maxVal) {
		mMaxValue = maxVal;
	}
	void printRawValues() {
		rt_printf("raw values: ");
		for (int i = 0; i < mRawValues.size(); ++i) {
			rt_printf("%f, ", mRawValues[i]);
		}
		rt_printf("\n");
	}
	void printNormValues() {
		rt_printf("norm values: ");
		for (int i = 0; i < mNormValues.size(); ++i) {
			rt_printf("%f, ", mNormValues[i]);
		}
		rt_printf("\n");
	}
	
	// Accessors
	float getCentroid() { return mCentroid; }
	float getPressure() { return mPressure; }
	bool  getTriggered() { return mImpulseDetector.isTriggered(); }
	int   getHoldTs() { return mImpulseDetector.getHoldTs(); }
	void  resetHoldTs(float ts) { mImpulseDetector.resetHoldTs(ts); }
	
	std::vector<int> ledPins;
	std::vector<std::vector<float>> sampleBuffers;
	
private:
	static constexpr auto MAX_PAD_VALUE = 0.82f;
	static constexpr auto TAP_THRESHOLD = 0.085f;

	float normalize(int idx, float input);
	
	std::vector<MuxConfig> mMuxPins;
	std::vector<float> mRawValues, mNormValues, mRestingValues;
	float mMaxValue = MAX_PAD_VALUE;
	float mCentroid = 0.0f; // Normalized from 0.0-1.0
	float mPressure = 0.0f; // Touch pressure
	ImpulseDetector mImpulseDetector; // Impuse detector for taps
	Interpolation<float> mInterpolator;
};